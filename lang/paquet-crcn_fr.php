<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// La base, SPIPr, est enrichie

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'crcn_description' => 'CRCN : Cadre de R&#233;f&#233;rence des Comp&#233;tences Num&#233;riques - Ce plugin va cr&#233;er automatiquement des groupes de mots-cl&#233;s et des mots-cl&#233;s correspondant aux diff&#233;rents domaines et comp&#233;tences du CRCN.
	Une fois attribu&#233;s &#224; un article du site, il est possible d\'afficher les comp&#233;tences gr&#226;ce au mod&#232;le <competences|>.',
	'crcn_slogan' => 'Attribuer et afficher les comp&#233;tences du CRCN',
);
?>